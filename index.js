var express = require('express');
var app = express();
var http = require('http');
var auctionModule = require("./auction");

app.get('/', function (req, res) {
    res.send(auctionModule.startAuction(
        [
            {name:'A', bids: [110,130]},
            {name:'B', bids: []},
            {name:'C', bids: [125]},
            {name:'D', bids: [105,115,90]},
            {name:'E', bids: [132,135,140]}
        ],
        100
    ));
})

http.createServer(app).listen("8100", function () {
    console.log('Express server listening on port 8100');
})
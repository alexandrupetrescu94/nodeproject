var assert = require('assert');
var auctionModule = require("../auction");
var messagesModule = require("../messages");

describe('Auction Module Test Cases', function() {
    describe('Participants > 1', function() {
    
        it('should return Second Highest Bid when Second Highest Bid > Reserve Price', function() {
            bids = [
                {name:'A', bids: [110,130]},
                {name:'B', bids: []},
                {name:'C', bids: [125]},
                {name:'D', bids: [105,115,90]},
                {name:'E', bids: [132,135,140]}
            ];
            reservePrice = 100;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.winner("E", 130)
            );
        });

        it('should return Reserve Price when Reserve Price > Second Highest Bid', function() {
            bids = [
                {name:'A', bids: [110,125]},
                {name:'B', bids: []},
                {name:'C', bids: [125]},
                {name:'D', bids: [105,115,90]},
                {name:'E', bids: [132,135,140]}
            ];
            reservePrice = 130;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice),
                messagesModule.messages.winner("E", 130)
            );
        });

        it('should return Reserve Price/Second Highest Bid when Reserve Price == Second Highest Bid', function() {
            bids = [
                {name:'A', bids: [110,125]},
                {name:'B', bids: []},
                {name:'C', bids: [125]},
                {name:'D', bids: [105,115,90]},
                {name:'E', bids: [132,135,140]}
            ];
            reservePrice = 125;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice),
                messagesModule.messages.winner("E", 125)
            );
        });

        it('should return No Winner if all the bids are below reservePrice', function() {
            bids = [
                {name:'A', bids: [110,125]},
                {name:'B', bids: []},
                {name:'C', bids: [125]},
                {name:'D', bids: [105,115,90]},
                {name:'E', bids: [132,135,140]}
            ];
            reservePrice = 150;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice),
                messagesModule.messages.noBiddingWinner
            );
        });

        it('should return No Bidder (participants) if no bids are in the arrays', function() {
            bidders = [
                {name:'A', bids: []},
                {name:'B', bids: []},
                {name:'C', bids: []},
                {name:'D', bids: []},
                {name:'E', bids: []}
            ];
            reservePrice = 150;
            assert.equal(
                auctionModule.startAuction(bidders, reservePrice),
                messagesModule.messages.noBidder
            );
        });

        it('should return the Winner if some of the objects are not well defined', function() {
            bids = [
                {},
                {bids: []},
                {name:'C', bids: [125]},
                {bids: [126]},
                {name:'E', bids: [130]}
            ];
            reservePrice = 120;
            assert.equal(
                auctionModule.startAuction(bids, reservePrice),
                messagesModule.messages.winner("E", 125)
            );
        });

    });
    describe('Participants = 1', function() {
    
        it('should return Reserve Price with multiple bids on participant', function() {
            bids = [
                {name:'A', bids: [110,130]}
            ];
            reservePrice = 100;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.winner("E", 100)
            );
        });

        it('should return No Bidder (participants) if no bids are in the arrays', function() {
            bidders = [
                {name:'A', bids: []},
            ];
            reservePrice = 150;
            assert.equal(
                auctionModule.startAuction(bidders, reservePrice),
                messagesModule.messages.noBidder
            );
        });

        it('should return the Winner if some of the objects are not well defined', function() {
            bids = [
                {},
                {bids: []},
                {bids: [126]},
                {name:'E', bids: [130]}
            ];
            reservePrice = 120;
            assert.equal(
                auctionModule.startAuction(bids, reservePrice),
                messagesModule.messages.winner("E", 120)
            );
        });

    });
    describe('Participants = 0', function() {
    
        it('should return No Bidder', function() {
            bids = [
            ];
            reservePrice = 100;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.noBidder
            );
        });

    });
    describe('Wrong Input Structure Cases', function() {
        it('should return Bidder Structure Error', function() {
            bids = {};
            reservePrice = 100;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.biddersStructureError
            );
        });
        it('should return Bidder Structure Error', function() {
            bids = null;
            reservePrice = 100;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.biddersStructureError
            );
        });
        it('should return Price Structure Error', function() {
            bids = [];
            reservePrice = "100";

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.reservePriceStructureError
            );
        });
        it('should return Price Structure Error', function() {
            bids = [];
            reservePrice = null;

            assert.equal(
                auctionModule.startAuction(bids, reservePrice), 
                messagesModule.messages.reservePriceStructureError
            );
        });
    })
});
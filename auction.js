var messagesModule = require("./messages");

/* Exposed method for starting auction */
var startAuction = function(inputBidders, inputReservePrice) {
    if (!Array.isArray(inputBidders)) {
        return messagesModule.messages.biddersStructureError;
    }
    if (inputReservePrice < 0 || typeof inputReservePrice != "number") {
        return messagesModule.messages.reservePriceStructureError;
    }

    bidders = inputBidders;
    reservePrice = inputReservePrice;

    /* Remove not participating bidders */
    participatingBidders = bidders.filter(
        (bidder) => {
            return bidder.hasOwnProperty("bids") && bidder.hasOwnProperty("name") ? bidder.bids.length > 0 : false;
        }
    );

    if (participatingBidders.length == 0) {
        return messagesModule.messages.noBidder;
    }
    /* One participant */
    if (participatingBidders.length == 1) {
        if (participatingBidders[0].bids[participatingBidders[0].bids.length - 1] > reservePrice)
            return messagesModule.messages.winner(sortedBids[0].name, reservePrice);
        else
            return messagesModule.messages.noBiddingWinner;
    }
    
    /* Multiple participants */
    // Get the last bid for each participant and sort the bids
    lastPhaseBid = participatingBidders.map((bidder) => {
        return {name: bidder.name, lastBid: bidder.bids[bidder.bids.length - 1]};
    });
    sortedBids = lastPhaseBid.sort(function(a,b){return a.lastBid < b.lastBid});
    
    if (sortedBids[0].lastBid < reservePrice) {
        return messagesModule.messages.noBiddingWinner;
    }
    
    winningPrice = sortedBids[1].lastBid >= reservePrice ? sortedBids[1].lastBid : reservePrice;     
    return messagesModule.messages.winner(sortedBids[0].name, winningPrice);
};

exports.startAuction = startAuction;
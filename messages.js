var messages = {
    "noBidder": "No Bidders",
    "noBiddingWinner": "No Bidders won the bid",
    "biddersStructureError": "Bidders should be an array",
    "reservePriceStructureError": "Reserve Price should be a number higher than 0",
    winner: function(name, price) {
        return  "The buyer " + name + " wins the auction at the price of " + price + " euros";
    }
};

exports.messages = messages;